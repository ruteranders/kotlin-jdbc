package no.ruter.opr.example.kotlinjdbc.repository

import no.ruter.opr.example.kotlinjdbc.domain.Customer
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.transaction.annotation.Transactional

@SpringBootTest
@Transactional
internal class CustomerRepositoryTest(
    @Autowired private val repository: CustomerRepository
) {

    @Test
    fun getAll() {
        val customers = repository.getAll()
        assertTrue(customers.isNotEmpty())
    }

    @Test
    fun get() {
        val customers = repository.getAll()
        val customer = repository.get(customers[0].id!!)
        assertNotNull(customer)
    }

    @Test
    fun insert() {
        val customerId = repository.insert(
            Customer(
                "Winnie the Pooh",
                "winnie"
            )
        )
        val customer = repository.get(customerId)
        assertEquals("winnie", customer?.username)
    }

    @Test
    fun update() {
        val customerId = repository.insert(
            Customer(
                "Winnie the Pooh",
                "winnie"
            )
        )
        val customer = repository.get(customerId)
        assertEquals("winnie", customer?.username)
        repository.update(customer!!.copy(username = "winnie2"))
        val updatedCustomer = repository.get(customerId)
        assertEquals("winnie2", updatedCustomer?.username)
        assertTrue(updatedCustomer!!.updatedAt!!.isAfter(customer.updatedAt))
    }
}