package no.ruter.opr.example.kotlinjdbc.config;

import org.flywaydb.core.Flyway
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class FlywayTestConfig {

    @Bean
    fun clean(): (Flyway) -> Unit =
        { flyway ->
            flyway.clean()
            flyway.migrate()
        }
}
