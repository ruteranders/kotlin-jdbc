SET search_path = main;

SELECT SETVAL('customer_id_seq', 1);

INSERT INTO customer (name, username, created_by, created_at, updated_by, updated_at)
VALUES ('John Doe', 'doe', 'system', current_timestamp, 'system', current_timestamp);
