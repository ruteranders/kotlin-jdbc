CREATE SCHEMA IF NOT EXISTS main;
SET search_path = main;

CREATE TABLE customer
(
    id           BIGINT GENERATED ALWAYS AS IDENTITY,
    name         VARCHAR                  NOT NULL,
    username     VARCHAR                  NOT NULL,
    created_by   VARCHAR                  NOT NULL,
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_by   VARCHAR                  NOT NULL,
    updated_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (id),
    UNIQUE (username)
);

CREATE TABLE customer_item
(
    id           BIGINT GENERATED ALWAYS AS IDENTITY,
    customer_id  BIGINT REFERENCES customer (id),
    content      VARCHAR                  NOT NULL,
    created_by   VARCHAR                  NOT NULL,
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_by   VARCHAR                  NOT NULL,
    updated_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE event
(
    id           BIGINT GENERATED ALWAYS AS IDENTITY,
    description  VARCHAR                  NOT NULL,
    created_by   VARCHAR                  NOT NULL,
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_by   VARCHAR                  NOT NULL,
    updated_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE customer_event
(
    customer_id  BIGINT REFERENCES customer (id),
    event_id     BIGINT REFERENCES event (id),
    created_by   VARCHAR                  NOT NULL,
    created_at   TIMESTAMP WITH TIME ZONE NOT NULL,
    PRIMARY KEY (customer_id, event_id)
);
