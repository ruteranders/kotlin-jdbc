package no.ruter.opr.example.kotlinjdbc.repository

import no.ruter.opr.example.kotlinjdbc.domain.Customer
import no.ruter.opr.example.kotlinjdbc.helper.EntityPropertySqlParameterSource
import org.springframework.jdbc.core.DataClassRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import javax.sql.DataSource
import org.springframework.stereotype.Repository as SpringRepository

@SpringRepository
class CustomerRepository(
    private val dataSource: DataSource,
    private val jdbcTemplate: NamedParameterJdbcTemplate
) : Repository<Customer> {

    val username = "somebody" // TODO: Retrieve from injected Principal or similar

    override fun getAll(): List<Customer> =
        jdbcTemplate.query(
            "SELECT * FROM customer",
            DataClassRowMapper.newInstance(Customer::class.java)
        )

    override fun get(id: Long): Customer? =
        jdbcTemplate.queryForObject(
            "SELECT * FROM customer WHERE id = :id",
            MapSqlParameterSource("id", id),
            DataClassRowMapper.newInstance(Customer::class.java)
        )

    override fun insert(item: Customer): Long =
        SimpleJdbcInsert(dataSource)
            .withTableName("customer")
            .usingGeneratedKeyColumns("id")
            .executeAndReturnKey(EntityPropertySqlParameterSource(item, username))
            .toLong()

    override fun update(item: Customer): Int =
        jdbcTemplate.update(
            """
                UPDATE customer
                   SET name = :name,
                       username = :username,
                       updated_by = :updatedBy,
                       updated_at = :updatedAt
                 WHERE id = :id""",
            EntityPropertySqlParameterSource(item, username)
        )
}
