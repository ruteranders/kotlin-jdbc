package no.ruter.opr.example.kotlinjdbc.repository

import no.ruter.opr.example.kotlinjdbc.domain.Entity

interface Repository<T : Entity> {

    fun getAll(): List<T> = throw RuntimeException("`getAll` is not implemented")

    fun get(id: Long): T? = throw RuntimeException("`get` is not implemented")

    fun insert(item: T): Long = throw RuntimeException("`insert` is not implemented")

    fun update(item: T): Int = throw RuntimeException("`update` is not implemented")
}
