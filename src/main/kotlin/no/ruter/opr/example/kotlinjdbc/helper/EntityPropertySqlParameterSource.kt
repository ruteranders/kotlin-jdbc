package no.ruter.opr.example.kotlinjdbc.helper

import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource
import java.time.LocalDateTime.now

class EntityPropertySqlParameterSource(source: Any, private val touchedBy: String) :
    BeanPropertySqlParameterSource(source) {

    override fun getValue(paramName: String): Any? =
        when (paramName) {
            "createdBy", "updatedBy" -> touchedBy
            "createdAt", "updatedAt" -> now()
            else -> super.getValue(paramName)
        }
}
