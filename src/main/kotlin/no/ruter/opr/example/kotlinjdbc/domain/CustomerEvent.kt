package no.ruter.opr.example.kotlinjdbc.domain

import java.time.LocalDateTime

data class CustomerEvent(
    val customerId: Long,
    val eventId: Long,
    override val createdBy: String? = null,
    override val createdAt: LocalDateTime? = null,
) : Entity
