package no.ruter.opr.example.kotlinjdbc.config

import org.springframework.boot.autoconfigure.jdbc.DataSourceProperties
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import javax.sql.DataSource

@Configuration
class DataSourceConfig {

    @Bean
    fun dataSource(properties: DataSourceProperties): DataSource? =
        properties.initializeDataSourceBuilder().build()
}