package no.ruter.opr.example.kotlinjdbc.repository

import no.ruter.opr.example.kotlinjdbc.domain.Event
import no.ruter.opr.example.kotlinjdbc.helper.EntityPropertySqlParameterSource
import org.springframework.jdbc.core.DataClassRowMapper
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.core.simple.SimpleJdbcInsert
import javax.sql.DataSource
import org.springframework.stereotype.Repository as SpringRepository

@SpringRepository
class EventRepository(
    private val dataSource: DataSource,
    private val jdbcTemplate: NamedParameterJdbcTemplate
) : Repository<Event> {

    val username = "somebody" // TODO: Retrieve from injected Principal or similar

    override fun getAll(): List<Event> =
        jdbcTemplate.query(
            "SELECT * FROM event",
            DataClassRowMapper.newInstance(Event::class.java)
        )

    override fun get(id: Long): Event? =
        jdbcTemplate.queryForObject(
            "SELECT * FROM event WHERE id = :id",
            MapSqlParameterSource("id", id),
            DataClassRowMapper.newInstance(Event::class.java)
        )

    override fun insert(item: Event): Long =
        SimpleJdbcInsert(dataSource)
            .withTableName("event")
            .usingGeneratedKeyColumns("id")
            .executeAndReturnKey(EntityPropertySqlParameterSource(item, username))
            .toLong()

    override fun update(item: Event): Int =
        jdbcTemplate.update(
            """
                UPDATE event
                   SET description = :description,
                       updated_by = :updatedBy,
                       updated_at = :updatedAt
                 WHERE id = :id""",
            EntityPropertySqlParameterSource(item, username)
        )
}
