package no.ruter.opr.example.kotlinjdbc.domain

import java.time.LocalDateTime

interface Entity {
    val createdBy: String?
    val createdAt: LocalDateTime?
}

interface IdentifiedEntity : Entity {
    val id: Long?
}

interface MutableEntity : Entity {
    val updatedBy: String?
    val updatedAt: LocalDateTime?
}

interface IdentifiedMutableEntity : MutableEntity {
    val id: Long?
}