package no.ruter.opr.example.kotlinjdbc.domain

import java.time.LocalDateTime

data class CustomerItem(
    val customerId: Long,
    val content: String,
    override val id: Long? = null,
    override val createdBy: String? = null,
    override val createdAt: LocalDateTime? = null,
    override val updatedBy: String? = null,
    override val updatedAt: LocalDateTime? = null,
) : IdentifiedMutableEntity
